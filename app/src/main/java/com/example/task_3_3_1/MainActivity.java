package com.example.task_3_3_1;

/*

3.1* Заменить три текстовых поля одним, в котором будет введено полностью выражение (например "2+2" или "16/4").
Добавить одну кнопку для расчета и вывода результата.

*/

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.task_3_3_1.interfaces.ArithmeticOperations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements ArithmeticOperations {

    EditText numbersAndOperation;
    TextView result;
    Button calc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numbersAndOperation = findViewById(R.id.numbersAndOperation);
        result = findViewById(R.id.result);
        calc = findViewById(R.id.calc);

        calc.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                calc(view);
            }
        });
    }

    //================================================================================//

    public void calc(View view) {

        String inputString = numbersAndOperation.getText().toString().trim();

        String regex = "(\\+|-|\\*|/)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(inputString);
        matcher.find();

        String arithmeticOperation = matcher.group();
        String[] operandsArray = inputString.split(regex);

        int firstNumberInt = Integer.parseInt(operandsArray[0]);
        int secondNumberInt = Integer.parseInt(operandsArray[1]);

        switch(arithmeticOperation){

            case ArithmeticOperations.ADDITION: {

                int resultInt = additionNumbers(firstNumberInt, secondNumberInt);
                result.setText(String.valueOf(resultInt));

                break;
            }

            case ArithmeticOperations.SUBTRACTION: {

                int resultInt = subtractionNumbers(firstNumberInt, secondNumberInt);
                result.setText(String.valueOf(resultInt));

                break;
            }

            case ArithmeticOperations.MULTIPLICATION: {

                int resultInt = multiplicationNumbers(firstNumberInt, secondNumberInt);
                result.setText(String.valueOf(resultInt));

                break;
            }

            case ArithmeticOperations.DIVISION: {

                double resultDouble = divisionNumbers(firstNumberInt, secondNumberInt);
                result.setText(String.valueOf(resultDouble));

                break;
            }

            default:
                break;
        }
    }

    //================================================================================//

    @Override
    public int additionNumbers(int numberFirst, int numberSecond) {
        return numberFirst + numberSecond;
    }

    @Override
    public int subtractionNumbers(int numberFirst, int numberSecond) {
        return numberFirst - numberSecond;
    }

    @Override
    public int multiplicationNumbers(int numberFirst, int numberSecond) {
        return numberFirst * numberSecond;
    }

    @Override
    public double divisionNumbers(int numberFirst, int numberSecond) {
        double resultDouble = 0;

        if (numberSecond == 0)
            //           Toast.makeText(this, "Second value can not be 0", Toast.LENGTH_LONG).show();

            new AlertDialog.Builder(this)
                    .setTitle("Error")
                    .setMessage("Second value can not be 0")
//            .setCancelable(false) // нескрываемый диалог :-))
                    // нужно обязательно выбрать какой-то вариант
//            .setPositiveButton("OK", null) // диалог закроется
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            numbersAndOperation.setText("");
                        }
                    }).show();
        else {
            resultDouble = ((double) numberFirst / (double) numberSecond);
//            result.setText(String.valueOf(firstNumberInt + secondNumberInt));
        }
        return resultDouble;
    }
}
