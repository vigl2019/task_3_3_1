package com.example.task_3_3_1.interfaces;

public interface ArithmeticOperations {
    String ADDITION = "+";
    String SUBTRACTION = "-";
    String MULTIPLICATION = "*";
    String DIVISION = "/";

    int additionNumbers(int numberFirst, int numberSecond);
    int subtractionNumbers(int numberFirst, int numberSecond);
    int multiplicationNumbers(int numberFirst, int numberSecond);
    double divisionNumbers (int numberFirst, int numberSecond);
}
